This is sample of MVP architecture project, which shows how to divide duties between Model - View - Presenter (Clean architecture).

App was made at learning course [http://skill-branch.ru](http://skill-branch.ru)


**Features**:

- imitation of catalog load and user enter

- validation of user fields and different types of animation

- support of orientation change