package yuribtr.skillbranch.ru.mvpauth.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import yuribtr.skillbranch.ru.mvpauth.BuildConfig;
import yuribtr.skillbranch.ru.mvpauth.R;
import yuribtr.skillbranch.ru.mvpauth.mvp.presenters.AuthPresenter;
import yuribtr.skillbranch.ru.mvpauth.mvp.presenters.IAuthPresenter;
import yuribtr.skillbranch.ru.mvpauth.mvp.views.IAuthView;
import yuribtr.skillbranch.ru.mvpauth.ui.custom_views.AuthPanel;

public class RootActivity extends AppCompatActivity implements IAuthView, View.OnClickListener{
    AuthPresenter mPresenter = AuthPresenter.getInstance();
    protected static ProgressDialog mProgressDialog;

    private int mEmailTextColor=Color.BLACK;
    private int mPasswordTextColor=Color.BLACK;
    private Animation  mAnimation;

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.auth_card)
    CardView mAuthCard;

    @BindView(R.id.auth_wrapper)
    AuthPanel mAuthPanel;

    @BindView(R.id.app_name_txt)
    TextView mAppName;

    @BindView(R.id.login_email_et)
    EditText mEmailText;

    @BindView(R.id.login_password_et)
    EditText mPasswordText;

    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;

    @BindView(R.id.login_btn)
    Button mLoginBtn;

    @BindView(R.id.vk_btn)
    ImageButton mVkBtn;

    @BindView(R.id.fb_btn)
    ImageButton mFbBtn;

    @BindView(R.id.tw_btn)
    ImageButton mTwBtn;

    @BindView(R.id.login_email_wrap)
    TextInputLayout mEmailWrap;

    @BindView(R.id.login_password_wrap)
    TextInputLayout mPasswordWrap;

    @BindView(R.id.enter_pb)
    ProgressBar mEnterProgressBar;

    //region ---------Lifecycle---------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);
        mPresenter.takeView(this);
        mPresenter.initView();

        mLoginBtn.setOnClickListener(this);
        mShowCatalogBtn.setOnClickListener(this);
        mVkBtn.setOnClickListener(this);
        mFbBtn.setOnClickListener(this);
        mTwBtn.setOnClickListener(this);
        mEmailTextColor = mEmailText.getCurrentTextColor();
        mPasswordTextColor = mPasswordText.getCurrentTextColor();
        //adding fonts
        Typeface myFontCondensed = Typeface.createFromAsset(getAssets(), "fonts/PTBebasNeueBook.ttf");
        Typeface myFontBold = Typeface.createFromAsset(getAssets(), "fonts/PTBebasNeueRegular.ttf");
        mAppName.setTypeface(myFontBold);
        //adding animation to social buttons
        mAnimation = (Animation) AnimationUtils.loadAnimation(this, R.anim.rotate);
        mVkBtn.setAnimation(mAnimation);
        mFbBtn.setAnimation(mAnimation);
        mTwBtn.setAnimation(mAnimation);
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    //endregion

    //region ---------IAuthView---------------

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.unknown_error));
            //todo:send error stacktrace to crashlytics
        }

    }

    @Override
    public void showLoad() {
        //showLoginProgress();
        if (mProgressDialog==null) {
            mProgressDialog=new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.show();
            mProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mProgressDialog.setContentView(R.layout.progress_splash);
        } else {
            mProgressDialog.show();
            mProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mProgressDialog.setContentView(R.layout.progress_splash);
        }

    }

    @Override
    public void hideLoad() {
        //hideLoginProgress();
        if (mProgressDialog!=null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.hide();
            }
        }
    }

    @Override
    public IAuthPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(View.GONE);
    }

    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }

    @Override
    public String getEmail() {
        return mEmailText.getText().toString();
    }

    @Override
    public String getPassword() {
        return mPasswordText.getText().toString();
    }

    @Override
    public void setNonAcceptableEmail() {
        mEmailText.setTextColor(Color.RED);
        //to workaround rotating
        if (!mPasswordText.getText().toString().isEmpty() || !mEmailText.getText().toString().isEmpty())
            mLoginBtn.setEnabled(false);
    }

    @Override
    public void setNonAcceptablePassword() {
        mPasswordText.setTextColor(Color.RED);
        //to workaround rotating
        if (!mPasswordText.getText().toString().isEmpty() || !mEmailText.getText().toString().isEmpty())
            mLoginBtn.setEnabled(false);
    }

    @Override
    public void setAcceptableEmail() {
        mEmailWrap.setErrorEnabled(false);
        mEmailText.setTextColor(mEmailTextColor);
        if (!mPasswordWrap.isErrorEnabled() && mPasswordText.getText()!=null && mPasswordText.getText().length()>0) mLoginBtn.setEnabled(true);
    }

    @Override
    public void setAcceptablePassword() {
        mPasswordWrap.setErrorEnabled(false);
        mPasswordText.setTextColor(mPasswordTextColor);
        if (!mEmailWrap.isErrorEnabled() && mEmailText.getText()!=null && mEmailText.getText().length()>0) mLoginBtn.setEnabled(true);
    }

    @Override
    public void setWrongEmailError() {
        mEmailWrap.setErrorEnabled(true);
        mEmailWrap.setError(getString(R.string.email_input_error));
        mLoginBtn.setEnabled(false);
    }

    @Override
    public void setWrongPasswordError() {
        mPasswordWrap.setErrorEnabled(true);
        mPasswordWrap.setError(getString(R.string.password_input_error));
        mLoginBtn.setEnabled(false);
    }

    @Override
    public void removeWrongEmailError() {
        mEmailWrap.setErrorEnabled(false);
    }

    @Override
    public void removeWrongPasswordError() {
        mPasswordWrap.setErrorEnabled(false);
    }

    //endregion


    @Override
    public void onBackPressed() {
        if (!mAuthPanel.isIdle()) {
            mAuthPanel.setCustomState(AuthPanel.IDLE_STATE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_catalog_btn:
                mPresenter.clickOnShowCatalog();
                break;
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                break;
            case R.id.fb_btn:
                v.startAnimation(mAnimation);
                mPresenter.clickOnFb();
                break;
            case R.id.vk_btn:
                v.startAnimation(mAnimation);
                mPresenter.clickOnVk();
                break;
            case R.id.tw_btn:
                v.startAnimation(mAnimation);
                mPresenter.clickOnTwitter();
                break;
        }

    }

    public void showLoginProgress(){
        mLoginBtn.setVisibility(View.GONE);
        mEnterProgressBar.setVisibility(View.VISIBLE);
    }

    public void hideLoginProgress(){
        mLoginBtn.setVisibility(View.VISIBLE);
        mEnterProgressBar.setVisibility(View.GONE);
    }


}
