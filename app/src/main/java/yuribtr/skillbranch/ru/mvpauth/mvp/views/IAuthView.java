package yuribtr.skillbranch.ru.mvpauth.mvp.views;

import android.content.Context;
import android.support.annotation.Nullable;

import yuribtr.skillbranch.ru.mvpauth.mvp.presenters.IAuthPresenter;
import yuribtr.skillbranch.ru.mvpauth.ui.custom_views.AuthPanel;

public interface IAuthView {

    void showMessage (String message);
    void showError (Throwable e);

    void showLoad();
    void hideLoad();

    IAuthPresenter getPresenter();

    void showLoginBtn();
    void hideLoginBtn();

    String getEmail();
    String getPassword();

    void setNonAcceptableEmail();
    void setNonAcceptablePassword();

    void setAcceptableEmail();
    void setAcceptablePassword();

    void setWrongEmailError();
    void setWrongPasswordError();

    void removeWrongEmailError();
    void removeWrongPasswordError();

    Context getContext();

    void showLoginProgress();
    void hideLoginProgress();

    @Nullable
    AuthPanel getAuthPanel();
}
