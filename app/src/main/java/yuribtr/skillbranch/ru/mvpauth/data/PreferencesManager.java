package yuribtr.skillbranch.ru.mvpauth.data;

import android.content.SharedPreferences;

import yuribtr.skillbranch.ru.mvpauth.utils.ConstantManager;
import yuribtr.skillbranch.ru.mvpauth.utils.MvpAuthApplication;

public class PreferencesManager {
    private SharedPreferences mSharedPreferences;

    public PreferencesManager() {
        this.mSharedPreferences= MvpAuthApplication.getSharedPreferences();
    }

    public void saveAuthToken (String authToken){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.AUTH_TOKEN, authToken);
        editor.apply();
    }

    public String getAuthToken (){
        return mSharedPreferences.getString(ConstantManager.AUTH_TOKEN, null);
    }

}
