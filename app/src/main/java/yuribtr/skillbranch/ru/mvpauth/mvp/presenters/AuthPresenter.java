package yuribtr.skillbranch.ru.mvpauth.mvp.presenters;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import yuribtr.skillbranch.ru.mvpauth.R;
import yuribtr.skillbranch.ru.mvpauth.mvp.models.AuthModel;
import yuribtr.skillbranch.ru.mvpauth.mvp.models.IAuthModel;
import yuribtr.skillbranch.ru.mvpauth.mvp.views.IAuthView;
import yuribtr.skillbranch.ru.mvpauth.ui.custom_views.AuthPanel;
import yuribtr.skillbranch.ru.mvpauth.utils.ConstantManager;

public class AuthPresenter implements IAuthPresenter{
    private static AuthPresenter ourInstance = new AuthPresenter();

    private IAuthModel mAuthModel;
    private IAuthView mAuthView;
    private boolean mIsCatalogLoading=false;

    private AuthPresenter() {
        mAuthModel = new AuthModel();
    }


    public static AuthPresenter getInstance(){
        return ourInstance;
    }

    @Override
    public void takeView(IAuthView authView) {
        mAuthView = authView;
    }

    @Override
    public void dropView() {
        mAuthView = null;
    }

    @Override
    public void initView() {
        if (getView()!=null) {
            if (checkUserAuth()) {
                getView().hideLoginBtn();
            } else {
                getView().showLoginBtn();
            }
            if (mIsCatalogLoading) getView().showLoad();
            else getView().hideLoad();

        }
    }

    @Nullable
    @Override
    public IAuthView getView() {
        return mAuthView;
    }

    @Override
    public void clickOnLogin() {
        if (getView()!=null && getView().getAuthPanel()!=null) {
            if (getView().getAuthPanel().isIdle()) {
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            } else {
                boolean emailOk = getView().getEmail().matches(ConstantManager.PATTERN_EMAIL);
                boolean passwordOk = getView().getPassword().matches(ConstantManager.PATTERN_PASSWORD);
                if (emailOk && passwordOk) {
                    getView().showMessage(getView().getContext().getString(R.string.user_authenticating_message));
                    getView().showLoginProgress();
                    mAuthModel.loginUser(getView().getAuthPanel().getUserEmail(), getView().getAuthPanel().getUserPassword());
                } else {
                    if (!emailOk) getView().setWrongEmailError();
                    if (!passwordOk) getView().setWrongPasswordError();
                    getView().showMessage(getView().getContext().getString(R.string.email_or_password_wrong_format));
                }
            }
        }
    }

    @Override
    public void clickOnFb() {
        if (getView()!=null) {
            getView().showMessage("clickOnFb");
        }
    }

    @Override
    public void clickOnVk() {
        if (getView()!=null) {
            getView().showMessage("clickOnVk");
        }
    }

    @Override
    public void clickOnTwitter() {
        if (getView()!=null) {
            getView().showMessage("clickOnTwitter");
        }
    }

    @Override
    public void clickOnShowCatalog() {
        mIsCatalogLoading = true;
        if (getView()!=null) {
            getView().showMessage(getView().getContext().getString(R.string.catalog_loading_message));
            getView().showLoad();
            class WaitSplash extends AsyncTask<Void, Void, Void> {
                protected Void doInBackground(Void... params) {
                    try {
                        Thread.currentThread();
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
                protected void onPostExecute(Void result) {
                    super.onPostExecute(result);
                    mIsCatalogLoading = false;
                    if (getView()!=null) {
                        getView().hideLoad();
                        getView().showMessage(getView().getContext().getString(R.string.catalog_loaded_message));
                    }
                }
            }
            WaitSplash waitSplash = new WaitSplash();
            waitSplash.execute();
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mAuthModel.isAuthUser();
    }

    @Override
    public void onPasswordChanged() {
        if (getView()!=null) {
            if (getView().getPassword().matches(ConstantManager.PATTERN_PASSWORD))
                getView().setAcceptablePassword();
            else
                getView().setNonAcceptablePassword();

        }
    }

    @Override
    public void onEmailChanged() {
        if (getView()!=null) {
            if (getView().getEmail().matches(ConstantManager.PATTERN_EMAIL))
                getView().setAcceptableEmail();
            else
                getView().setNonAcceptableEmail();
        }
    }

    @Override
    public void onLoginSuccess() {
        if (getView()!=null && getView().getAuthPanel()!=null) {
            getView().showMessage(getView().getContext().getString(R.string.authentificate_successful));
            getView().hideLoginProgress();
            getView().hideLoginBtn();
            getView().getAuthPanel().setCustomState(AuthPanel.IDLE_STATE);
        }
    }

    @Override
    public void onLoginError(String message) {
        if (getView()!=null) {
            getView().hideLoginProgress();
            getView().showMessage(message);
        }
    }
}
