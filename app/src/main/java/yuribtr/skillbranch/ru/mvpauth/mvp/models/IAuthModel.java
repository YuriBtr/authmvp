package yuribtr.skillbranch.ru.mvpauth.mvp.models;

public interface IAuthModel {

    boolean isAuthUser();
    void loginUser (String email, String password);

}
