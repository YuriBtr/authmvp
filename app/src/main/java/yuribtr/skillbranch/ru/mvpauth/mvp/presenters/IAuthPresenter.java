package yuribtr.skillbranch.ru.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import yuribtr.skillbranch.ru.mvpauth.mvp.views.IAuthView;

public interface IAuthPresenter {

    void takeView(IAuthView authView);
    void dropView();
    void initView();

    @Nullable
    IAuthView getView();

    void clickOnLogin();
    void clickOnFb();
    void clickOnVk();
    void clickOnTwitter();
    void clickOnShowCatalog();

    void onPasswordChanged();
    void onEmailChanged();

    boolean checkUserAuth();

    void onLoginSuccess();
    void onLoginError(String message);
}
