package yuribtr.skillbranch.ru.mvpauth.utils;

public interface ConstantManager {
    String PATTERN_EMAIL = "^[a-zA-Z_0-9]{3,}@[a-zA-Z_0-9.]{2,}\\.[a-zA-Z0-9]{2,}$";
    String PATTERN_PASSWORD = "^\\S{8,}$";
    String AUTH_TOKEN = "AUTH_TOKEN";
}
